import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Networker {
	
	OkHttpClient client = new OkHttpClient.Builder()
		    .cookieJar(new NonPersistentCookieJar())
		    .build();
	
	
	
	public Networker(){

	}
	
	Response response;
	Request request;
	

	String askLink(String url) throws IOException {
		
		request = new Request.Builder()
	            .url(url)
	            .build();

	    response = client.newCall(request).execute();
	   
	   return response.body().string();
	}		
	
	String askJson(String url){
		 
		    
		return null;
	}

}

class NonPersistentCookieJar implements CookieJar {
    private final Set<Cookie> cookieStore = new LinkedHashSet<>();

    @Override
    public synchronized void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
        cookieStore.addAll(cookies);
    }

    @Override
    public synchronized List<Cookie> loadForRequest(HttpUrl url) {
        List<Cookie> matchingCookies = new ArrayList<>();
        Iterator<Cookie> it = cookieStore.iterator();
        while (it.hasNext()) {
            Cookie cookie = it.next();
            if (cookie.expiresAt() < System.currentTimeMillis()) {
                it.remove();
            } else if (cookie.matches(url)) {
                matchingCookies.add(cookie);
            }
        }
        return matchingCookies;
    }
}
