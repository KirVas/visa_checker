import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.io.*;
import javax.net.ssl.HttpsURLConnection;

public class Helper{
	
	private static HttpsURLConnection con;
	String location;
	InputStream is = null;
	
	public Helper(String https_url){

		URL url;
	      try {

	    	  url = new URL(https_url);
	    	  con = (HttpsURLConnection)url.openConnection();
	    	  
	    	  con.setConnectTimeout(15000);
	    	  con.setReadTimeout(15000);
	    	  con.setInstanceFollowRedirects(false);   // Make the logic below easier to detect redirections
	    	  con.setRequestProperty("User-Agent", "Mozilla/5.0...");
	    	  is = con.getInputStream();
	    	  	    	  
				
	      } catch (MalformedURLException e) {
	    	  e.printStackTrace();
	      } catch (IOException e) {
	    	  e.printStackTrace();
	      }
	    

	}
	
//	public void redirrect(String url) throws Exception, IOException{
//		
//		String cookies = con.getHeaderField("Set-Cookie");
//		System.out.println(cookies);
//		con = (HttpsURLConnection) new URL(url).openConnection();
//		con.setRequestProperty("Cookie", "AppCookieName=cd2c2opir51ttf003nngpluf7ehim210; hl=ru");//cookies);
//		con.addRequestProperty("Host", "evas2.urm.lt");
//		con.addRequestProperty("Connection", "keep-alive");
//		con.addRequestProperty("Cache-Control", "max-age=0");
//		con.addRequestProperty("Upgrade-Insecure-Requests", "1");
//		con.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36");
//		con.addRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
//		con.addRequestProperty("Referer", "https://evas2.urm.lt/ru/");
//		con.addRequestProperty("Accept-Encoding", "gzip, deflate, br");
//		con.addRequestProperty("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
//		con.addRequestProperty("Referrer Policy", "no-referrer-when-downgrade");
////		con.setRequestProperty("", "");
////		con.setRequestProperty("", "");
////		con.setRequestProperty("", "");
//	
//	}
	
	
	public void redirrect(String url) throws Exception{
		switch (con.getResponseCode())
	     {
	        case HttpsURLConnection.HTTP_MOVED_PERM:
	        case HttpsURLConnection.HTTP_MOVED_TEMP:
	           location = con.getHeaderField("Location");
	           location = URLDecoder.decode(location, "UTF-8");
	           URL base     = new URL(url);               
	           URL next     = new URL(base, location);  // Deal with relative URLs
	           url      = next.toExternalForm();
	           
	     }
	}
	
   
   
   public String getIt(){
	   if(con!=null){
			
		   try {

			   BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
			    StringBuilder response = new StringBuilder(); 
			    String line;
			    while ((line = rd.readLine()) != null) {
			      response.append(line);
			      response.append('\r');
			    }
			    rd.close();
			    return response.toString();
		   } catch (IOException e) {
			   e.printStackTrace();
		   }
			
	   }
	   return null;
   	}
}